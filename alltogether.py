from urllib.request import urlopen
story = urlopen('https://sixty-north.com/c/t.txt')
story_words = []
for line in story:
    line_words = line.decode('utf-8').split()
    #print(line_words)
    for word in line_words:
        story_words.append(word)
story.close()
print(story_words)
#what this program does is it first fetches a line in outerloop, and then this line is fed into the inner loop
#then inner loop  fetches each word in the line and append it in list story_words 